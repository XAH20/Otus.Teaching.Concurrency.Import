﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.DataContexts;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath;
        private static int _dataCount;
        private static int _threadsCount;
        private static bool _isRunGeneratorInNewProcess;
        private static readonly string _dataFileDirectoryDefault = AppDomain.CurrentDomain.BaseDirectory;
        private static bool _loadInSqlite;
        private static bool _loadInPostgresql;
        private static bool _isUseThreadPool;

        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            Console.WriteLine($"Program started with process Id {Process.GetCurrentProcess().Id}...");

            if (!TryValidateAndParseArgs(args))
            {
                return;
            }

            GenerateData();
            var customers = ParseDataFile();
            
            LoadData(customers);

            stopwatch.Stop();
            Console.WriteLine($"Program running time: {stopwatch.Elapsed}");
        }

        private static void GenerateData()
        {
            if (_isRunGeneratorInNewProcess)
            {
                GenerateCustomersDataFileInNewProcess();
            }
            else
            {
                GenerateCustomersDataFile();
            }
        }

        private static void LoadData(List<Customer> customers)
        {
            var stopwatchLoading = new Stopwatch();
            stopwatchLoading.Start();

            if (_loadInSqlite)
            {
                LoadInSqlite(customers);
            }
            else if (_loadInPostgresql)
            {
                LoadInPostgresql(customers);
            }
            else
            {
                LoadFake(customers);
            }

            stopwatchLoading.Stop();
            Console.WriteLine($"Data loading time: {stopwatchLoading.Elapsed}");
        }

        private static void LoadInSqlite(List<Customer> customers)
        {
            var loader = new CustomerLoader<SqliteCustomerRepository>(customers, _threadsCount);
            if (_isUseThreadPool)
            {
                loader.OptionThreadLoading = OptionThreadLoading.ThreadPool;
            }
            using var dataContext = new SqliteDataContext();
            InitializeDb(dataContext);
            loader.LoadData();
        }

        private static void LoadInPostgresql(List<Customer> customers)
        {
            var loader = new CustomerLoader<PostgresqlCustomerRepository>(customers, _threadsCount);
            if (_isUseThreadPool)
            {
                loader.OptionThreadLoading = OptionThreadLoading.ThreadPool;
            }
            using var dataContext = new PostgresqlDataContext();
            InitializeDb(dataContext);
            loader.LoadData();
        }

        private static void LoadFake(List<Customer> customers)
        {
            var loader = new CustomerLoader<CustomerRepository>(customers, _threadsCount);
            if (_isUseThreadPool)
            {
                loader.OptionThreadLoading = OptionThreadLoading.ThreadPool;
            }
            loader.LoadData();
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                if (args[0].Contains(":"))
                {
                    _dataFilePath = args[0];
                }
                else
                {
                    _dataFilePath = Path.Combine(_dataFileDirectoryDefault, $"{args[0]}");
                }
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }

            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out _dataCount))
                {
                    Console.WriteLine("Filename data must be integer");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("Must specify data count");
                return false;
            }

            if (args.Length > 2)
            {
                if (!int.TryParse(args[2], out _threadsCount))
                {
                    Console.WriteLine("Threads count must be integer");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("Must specify threads count");
                return false;
            }

            if (args.Contains("-p"))
            {
                _isRunGeneratorInNewProcess = true;
            }

            if (args.Contains("-sqlite"))
            {
                _loadInSqlite = true;
            }
            else if (args.Contains("-pgsql"))
            {
                _loadInPostgresql = true;
            }

            if (args.Contains("-pool"))
            {
                _isUseThreadPool = true;
            }

            return true;
        }

        static void InitializeDb(DbContext dataContext)
        {
            Console.WriteLine("Initializing data base...");
            new DbInitializer(dataContext).Initialize();
        }

        static void GenerateCustomersDataFile()
        {
            Console.WriteLine($"Data generator started with process Id {Process.GetCurrentProcess().Id}...");
            Console.WriteLine("Generating data...");
            var generator = GeneratorFactory.GetGenerator(_dataFilePath, _dataCount);
            generator.Generate();
            Console.WriteLine($"Generated data in {_dataFilePath}.");
        }

        static void GenerateCustomersDataFileInNewProcess()
        {
            string filenameDataGenerator;
#if DEBUG
            filenameDataGenerator = @"..\..\..\..\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
#else
            filenameDataGenerator = new FileInfo(@"..\..\..\..\..\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Release\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe").FullName;
#endif
            var process = new Process 
            {StartInfo = 
                {FileName = filenameDataGenerator,
                Arguments = $"{_dataFilePath} {_dataCount}"
                }
            };

            process.Start();
            Console.WriteLine($"Data generator started with process Id {process.Id}...");
            process.WaitForExit();
            process.Close();
        }

        static List<Customer> ParseDataFile()
        {
            var parser = ParserFactory.GetParser(_dataFilePath);
            return parser.Parse();
        }
    }
}
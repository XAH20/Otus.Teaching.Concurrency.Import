using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public static class ParserFactory
    {
        public static IDataParser<List<Customer>> GetParser(string fileName)
        {
            if (fileName.EndsWith(".xml"))
            {
                return new XmlParser(fileName);
            }
            if(fileName.EndsWith(".csv"))
            {
                return new CsvParser(fileName);
            }

            throw new ArgumentException("Invalid file extension specified");
        }
    }
}
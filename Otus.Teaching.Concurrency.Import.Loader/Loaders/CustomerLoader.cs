using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class CustomerLoader<T> : DataLoader<Customer> where T : ICustomerRepository, IDisposable, new()
    {
        private static readonly int _attemptLoadLimit = 10;
        private static int _partIdNext = 1;
        private static readonly object _partIdNextLocker = new object();

        public CustomerLoader(IEnumerable<Customer> customers, int threadsCount) : base(customers, threadsCount)
        {
        }
        
        protected override void LoadPartData(object data)
        {
            var customers = (IEnumerable<Customer>)data;

            int partId;
            lock (_partIdNextLocker)
            {
                partId = _partIdNext++;
            }

            var countCustomersLoaded = 0;
            var countCustomers = customers.Count();
            var interval = Math.Max(10, countCustomers / 20);

            using var repository = new T();
            foreach (var item in customers)
            {
                LoadCustomer(repository, item);
                countCustomersLoaded++;
                if (countCustomersLoaded % interval == 0)
                {
                    Console.WriteLine($"Loaded {countCustomersLoaded} customers of {countCustomers} in part #{partId}");
                }
            }
        }

        private void LoadCustomer(T repository, Customer customer)
        {
            Exception exception;
            for (var i = 1; i <= _attemptLoadLimit; i++)
            {
                if (TryLoadCustomer(repository, customer, out exception))
                {
                    break;
                }

                Console.WriteLine($"Unsuccessful attempt to load customer: {customer}.");

                if (i < _attemptLoadLimit)
                {
                    Console.WriteLine($"Attempt #{i + 1} to load customer: {customer}.");
                }
                else
                {
                    Console.WriteLine($"Unsuccessful load customer: {customer}.");
                    Console.WriteLine(exception);
                }
            }
        }
        
        private bool TryLoadCustomer(T repository, Customer item, out Exception exception)
        {
            try
            {
                repository.AddCustomer(item);
                exception = null;
                return true;
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }
        }
    }
}
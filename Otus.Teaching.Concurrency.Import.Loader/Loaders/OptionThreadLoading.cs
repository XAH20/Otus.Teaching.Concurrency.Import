namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public enum OptionThreadLoading
    {
        Threads,
        ThreadPool
    }
}
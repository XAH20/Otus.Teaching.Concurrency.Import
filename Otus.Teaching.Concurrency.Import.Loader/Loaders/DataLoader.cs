using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public abstract class DataLoader<T> : IDataLoader
    {
        private readonly IEnumerable<T> _data;
        private int _threadsCount;
        public OptionThreadLoading OptionThreadLoading { get; set; }

        public DataLoader(IEnumerable<T> data, int threadsCount)
        {
            _data = data;
            _threadsCount = threadsCount;
        }

        public void LoadData()
        {
            Console.WriteLine("Loading data...");

            var partsData = Split(_data, _threadsCount);
            LoadPartsData(partsData);

            Console.WriteLine("Loaded data.");
        }

        private void LoadPartsData(IEnumerable<IEnumerable<T>> partsData)
        {
            switch (OptionThreadLoading)
            {
                case OptionThreadLoading.Threads:
                    LoadPartsDataThreads(partsData);
                    break;
                case OptionThreadLoading.ThreadPool:
                    LoadPartsDataThreadPool(partsData);
                    break;
            }
        }

        private void LoadPartsDataThreads(IEnumerable<IEnumerable<T>> partsData)
        {
            var threads = new List<Thread>();

            foreach (var part in partsData)
            {
                var thread = new Thread(LoadPartData);
                threads.Add(thread);
                thread.Start(part);
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }
        }
        
        private void LoadPartsDataThreadPool(IEnumerable<IEnumerable<T>> partsData)
        {
            var areList = new List<AutoResetEvent>();
            foreach (var part in partsData)
            {
                var are = new AutoResetEvent(false);
                areList.Add(are);
                ThreadPool.QueueUserWorkItem(LoadPartDataThreadPool, new {data = part, are });
            }
            WaitHandle.WaitAll(areList.ToArray());
        }

        public static IEnumerable<IEnumerable<T>> Split (IEnumerable<T> list, int parts)
        {
            var i = 0;
            var splits = from item in list
                group item by i++ % parts
                into part
                select part.AsEnumerable();
            return splits;
        }

        protected abstract void LoadPartData(object data);
        
        private void LoadPartDataThreadPool(object state)
        {
            dynamic o = state;
            var data = (IEnumerable<T>)o.data;
            var are = (AutoResetEvent)o.are;
            LoadPartData(data);
            are.Set();
        }
    }
}
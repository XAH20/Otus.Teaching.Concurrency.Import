using System;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.DataGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, int dataCount)
        {
            if (fileName.EndsWith(".xml"))
            {
                return new XmlDataGenerator(fileName, dataCount);
            }
            if(fileName.EndsWith(".csv"))
            {
                return new CsvGenerator(fileName, dataCount);
            }

            throw new ArgumentException("Invalid file extension specified");
        }
    }
}
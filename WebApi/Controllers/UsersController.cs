﻿using System.Diagnostics.Eventing.Reader;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.DataContexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private CustomerDataContextBase _customerDataContextBase;

        public UsersController(CustomerDataContextBase customerDataContextBase)
        {
            _customerDataContextBase = customerDataContextBase;
        }

        // GET api/<UsersController>/5
        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>user</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Customer>> Get(int id)
        {
            var customer = await _customerDataContextBase.Customers.FirstOrDefaultAsync(c => c.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // POST api/<UsersController>
        /// <summary>
        /// Add user
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>result</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Post([FromBody] Customer user)
        {
            if (_customerDataContextBase.Customers.AnyAsync(c => user.Id == c.Id).Result)
            {
                return Conflict();
            }

            _customerDataContextBase.Customers.Add(user);
            await _customerDataContextBase.SaveChangesAsync();
            return Ok();
        }
    }
}
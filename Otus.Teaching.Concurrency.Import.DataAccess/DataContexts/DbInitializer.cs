﻿using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.DataAccess.DataContexts
{
    public class DbInitializer
    {
        private readonly DbContext _dbContext;

        public DbInitializer(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Initialize()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Database.EnsureCreated();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.DataContexts
{
    public abstract class CustomerDataContextBase : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
    }
}
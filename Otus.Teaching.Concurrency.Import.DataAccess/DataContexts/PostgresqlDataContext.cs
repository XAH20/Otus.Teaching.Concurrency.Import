﻿using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.DataAccess.DataContexts
{
    public class PostgresqlDataContext : CustomerDataContextBase
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=Customers; User Id=postgres;Password=123");
        }
    }
}
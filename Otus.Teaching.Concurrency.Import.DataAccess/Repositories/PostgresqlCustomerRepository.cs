using Otus.Teaching.Concurrency.Import.DataAccess.DataContexts;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class PostgresqlCustomerRepository : CustomerRepositoryBase<PostgresqlDataContext>
    {
    }
}
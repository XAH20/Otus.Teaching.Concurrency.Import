using System;
using Otus.Teaching.Concurrency.Import.DataAccess.DataContexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public abstract class CustomerRepositoryBase<T> : ICustomerRepository, IDisposable where T : CustomerDataContextBase, new()
    {
        public T DataContext { get; }
        public CustomerRepositoryBase()
        {
            DataContext = new T();
        }

        public void AddCustomer(Customer customer)
        {
            DataContext.Customers.Add(customer);
            DataContext.SaveChanges();
        }

        public void Dispose()
        {
            DataContext?.Dispose();
        }
    }
}
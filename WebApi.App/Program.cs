﻿using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace WebApi.App
{
    class Program
    {
        private static string _api_users = "https://localhost:5001/api/users";
        private static HttpClient _httpClient = new();
        private static Random random = new();

        static async Task Main(string[] args)
        {
            await RunApp();
        }

        static async Task RunApp()
        {
            var startMassage = @"
    Using:
    To get a user, enter <number id>.
    To add a random user to the database, enter 'add'.
    To exit, enter 'exit'";
            Console.WriteLine(startMassage + "\n");
            while (true)
            {
                Console.Write("\nInput: ");
                var input = Console.ReadLine();
                switch (input)
                {
                    case "add":
                        await AddUser();
                        break;
                    case "exit":
                        return;
                    default:
                        if (int.TryParse(input, out int id))
                        {
                            await GetUser(id);
                        }
                        else
                        {
                            Console.WriteLine("Incorrect input");
                        }
                        break;
                }
            }
        }

        static async Task AddUser()
        {
            var customer = RandomCustomerGenerator.Generate(1)[0];
            //customer.Id = random.Next(100);
            Console.WriteLine($"Generated user: {JsonSerializer.Serialize(customer)}");
            var stringContent = new StringContent(JsonSerializer.Serialize(customer, customer.GetType()), Encoding.UTF8, "application/json");
            Console.WriteLine("Addition user..");
            var response = await _httpClient.PostAsync(_api_users, stringContent);
            Console.WriteLine($"Result: {response.StatusCode}");
        }

        static async Task GetUser(int id)
        {
            var request = $"{_api_users}/{id}";
            var response = await _httpClient.GetAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var user = await response.Content.ReadAsStringAsync();
                Console.WriteLine($"Received user: {user}");
            }
            else
            {
                Console.WriteLine($"Error: {response.StatusCode}");
            }
        }
    }
}
 